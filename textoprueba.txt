Reina en mi esp�ritu una alegr�a admirable, 
muy parecida a las dulces alboradas de la primavera, 
de que gozo aqu� con delicia. Estoy solo, 
y me felicito de vivir en este pa�s, 
el m�s a prop�sito para almas como la m�a, 
soy tan dichoso, mi querido amigo, 
me sojuzga de tal modo la idea de reposar, 
que no me ocupo de mi arte. 
Ahora no sabr�a dibujar, ni siquiera hacer una l�nea con el l�piz; 
y, sin embargo, jam�s he sido mejor pintor 
Cuando el valle se vela en torno m�o con un encaje de vapores; 
RODRIGO UN SE�OR
cuando siento m�s cerca de mi coraz�n los rumores de vida 
de ese peque�o mundo que palpita en los tallos de las hojas, 
y veo las formas innumerables e infinitas de los gusanillos y 
de los insectos; cuando siento, en fin, la presencia del 
Todopoderoso, que nos ha creado

Hola qu� tal, soy el chico de las poes�as

aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa
bbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbb
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
ccccccccccccccccccccccccccccccccccccccccccccccc